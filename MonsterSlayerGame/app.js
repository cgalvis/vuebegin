new Vue({
  el: "#app",
  data: {
    playerHealth: 100,
    monsterHealth: 100,
    gameIsRunning: false,
    turns: [],
  },
  methods: {
    startGame: function () {
      this.gameIsRunning = true;
      this.playerHealth = 100;
      this.monsterHealth = 100;
    },
    attack: function () {
      if (this.playerAttackWin()) {
        return;
      }
      if (this.monsterAttackWin()) {
        return;
      }
    },
    specialAttack: function () {
      if (this.playerAttackWin(10, 20, (specialAttack = true))) {
        return;
      }
      if (this.monsterAttackWin()) {
        return;
      }
    },
    heal: function () {
      this.playerHealth <= 90
        ? (this.playerHealth += 10)
        : (this.playerHealth = 100);
      this.updateTurnsLogs(true, "Player heals for 10");
      if (this.monsterAttackWin()) {
        return;
      }
    },
    restartTurns: function () {
      this.turns = [];
    },
    giveUp: function () {
      this.gameIsRunning = false;
      this.restartTurns();
    },
    calculeDamage: function (minDamage, maxDamage) {
      return Math.max(Math.floor(Math.random() * maxDamage) + 1, minDamage);
    },
    updateTurnsLogs: function (isPlayer, text) {
      this.turns.unshift({
        isPlayer: isPlayer,
        text: text,
      });
    },
    checkWin: function () {
      alertFinishGame = (phrase) => {
        confirm(`You ${phrase}!!! New Game?`)
          ? this.startGame()
          : (this.gameIsRunning = false);
        this.restartTurns();
      };
      if (this.monsterHealth <= 0) {
        alertFinishGame("won");
        return true;
      } else if (this.playerHealth <= 0) {
        alertFinishGame("lost");
        return true;
      }
      return false;
    },
    monsterAttackWin: function (minDamage = 5, maxDamage = 12) {
      let damage = this.calculeDamage(minDamage, maxDamage);
      this.playerHealth -= damage;
      this.updateTurnsLogs(false, "Monster hits Player for " + damage);
      return this.checkWin();
    },
    playerAttackWin: function (
      minDamage = 3,
      maxDamage = 10,
      specialAttack = false
    ) {
      let damage = this.calculeDamage(minDamage, maxDamage);
      this.monsterHealth -= damage;
      specialAttack
        ? this.updateTurnsLogs(true, "Player hits Monster hard for " + damage)
        : this.updateTurnsLogs(true, "Player hits Monster for " + damage);
      return this.checkWin();
    },
  },
});
